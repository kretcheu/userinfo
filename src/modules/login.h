/*
    Copyright (C) 2001-2015 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef LOGIN_H
#define LOGIN_H

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifdef HAVE_ERR_H
#include <err.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifdef HAVE_PATHS_H
#include <paths.h>
#else
#ifndef _PATH_DEV
#define _PATH_DEV	"/dev/"
#endif
#endif

#ifdef HAVE_LASTLOG_H
#include <lastlog.h>
#endif

#ifndef _PATH_LASTLOG
#define _PATH_LASTLOG	"/var/adm/lastlog"
#endif

#ifdef HAVE_UTMPX_H
#include <utmpx.h>
#ifndef UT_HOSTSIZE
#define UT_HOSTSIZE 256
#define UT_LINESIZE 32
#define UT_NAMESIZE 32
#ifndef UTX_NAMESIZE
#ifdef UTX_USERSIZE
#define UTX_NAMESIZE UTX_USERSIZE
#endif
#endif
#endif
typedef struct utmpx UTMP;
#else
#ifdef HAVE_UTMP_H
#include <utmp.h>
typedef struct utmp UTMP;
#endif
#endif

#define TIMEBUFSIZE	64

#ifdef HAVE_PROCFS
#ifdef HAVE_DIRENT_H
#include <dirent.h>
static DIR *procdir;
#endif
#endif

#ifdef HAVE_KVM_H
#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/user.h>
#include <kvm.h>
static kvm_t *kd;
#endif

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#endif
