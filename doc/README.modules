Writing Userinfo Modules
------------------------

This document describes how to write your own modules for use with the
userinfo utility.
    
Here are the available functions for modules:

    void (ui_module_init)(int *chainable);
	Setup any defaults or anything that needs to be initialized for your
	module here. The only parameter is a pointer to an integer which
	should be set to 1 if the module is chainable and 0 if not. Chainable
	means the previous module was passed the -x or -X option and this
	module is at the middle or end of the module chain.

    void (ui_module_exit)(void);
        Your module should free any resources allocated and close any files
	here. This is called just before the main program exits.

    void *(ui_module_help)(void);
	This displays help on the command line. Put a bunch of printf()'s in
	here to show available command-line options for your module. Try to
	keep the help output consistant with other modules (see -h).

    char *(ui_module_options_init)(char **defaults);
        This function should return an option string which is compatible with
	getopt(3) for any options your module may take. The "defaults"
	argument should be the default options you'd want specified (only
	options without option arguments are supported) if none are specified
	for this module on the command line or in the configuration file. If
	your module takes no options then return NULL; this will bypass any
	ui_module_options() calls (see below). If you do not want to set any
	default options, then set "defaults" to NULL.

    int (ui_module_options)(int argc, char **argv);
	This function should parse your module command-line options via
	getopt(3). It takes two arguments which are the argument count and
	argument list as if passed to the main() function.
        
    int (ui_module_exec)(char ***, const struct passwd *, const int, const int,
            char *);
	This is the main module function. Each module has this function called
	in sequence of other modules and not directly after the option
	parsing. This should be taken into consideration when loading a module
	more than once. It takes a few arguments:
	    1 - A pointer to an array of character pointers holding strings
	        which will be output in the order they have been added. If
		this parameter is initialized (not NULL), then chaining has
		been requested for this module (either -x or -X from the
		command-line, or '>' or '-' from the configuration file).

	    2 - A password structure for the current username.

	    3 - A character which separates multi-string values. This can be
		specified on the command line (-m). For example, the password
		module can output all groups a user belongs to. These groups
		are separated by a comma (by default) rather than the field
		deliminator (-F).

	    4 - Whether the -v option was passed on the command line
	        (verbose). Useful if you want to limit the display unless
		otherwise needed.

	    5 - The strftime() time format. This is either the default or the
	        value of the -t command line option. If you have any values
		that contain seconds-from-epoch, you should pass this format
		string to strftime() to maintain output consistency.

	This function should return an integer 0 on success or 1 on general
	failure. Note that this affects the program exit status.

	The output for fields which contain static information should be
	consistant with the hardcoded values in the main userinfo utility.
	Type 'ui -h' and look for "Output Key" and assign values for the
	static fields from this key.

Note that the order of output is dependent on the order of module loading and
the order of module options.

There are three other functions that are built into the userinfo utility which
can be used in your module. The first is a function to add a character string
to an array of character pointers:

    void add_string(char ***dest, const char *string);

The first argument is the destination buffer and the second is the string to
add. 

The second function is a time format conversion function. It looks like this:

    char *stamp(time_t epoch, const char *format);

If you have any information arguments that are time strings, you should try
and convert them to seconds-since-epoch then run them through the above
function. This keeps the time format consistant with other modules (-t command
line option).

Finally, a safer version of strncat(3) that will always nul-terminate the
destination buffer by treating the size N as N-1. Both the dst and src strings
must be nul terminated:

    char *safe_strncat(char *dst, const char *src, size_t n);

If you have any questions, feel free to send them to the address below.

Ben Kibbey <bjk@luxsci.net>
